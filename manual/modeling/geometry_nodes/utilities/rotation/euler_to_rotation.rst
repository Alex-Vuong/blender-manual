.. index:: Geometry Nodes; Euler to Rotation
.. _bpy.types.FunctionNodeEulerToRotation:

**********************
Euler to Rotation Node
**********************

.. figure:: /images/node-types_FunctionNodeEulerToRotation.png
   :align: right
   :alt: Euler to Rotation node.

The *Euler to Rotation* node creates a rotation value from an Euler rotation.

Inputs
======

Euler
    The :term:`Euler` rotation.


Outputs
=======

Rotation
    Standard rotation value.
