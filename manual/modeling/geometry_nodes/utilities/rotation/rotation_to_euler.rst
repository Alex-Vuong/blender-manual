.. index:: Geometry Nodes; Roation to Euler
.. _bpy.types.FunctionNodeRotationToEuler:

**********************
Rotation to Euler Node
**********************

.. figure:: /images/node-types_FunctionNodeRotationToEuler.png
   :align: right
   :alt: Rotation to Euler node.

The *Rotation to Euler* node converts a standard rotation socket value to an Euler rotation.

Inputs
======

Rotation
    Standard rotation socket value.


Outputs
=======

Euler
    The :term:`Euler` rotation.
