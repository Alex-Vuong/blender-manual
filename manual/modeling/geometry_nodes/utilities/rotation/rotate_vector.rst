.. index:: Geometry Nodes; Rotate Vector
.. _bpy.types.FunctionNodeRotateVector:

******************
Rotate Vector Node
******************

.. figure:: /images/node-types_FunctionNodeRotateVector.png
   :align: right
   :alt: Rotate Vector node.

The *Rotate Vector* node rotates a vector by a given rotation value.

Inputs
======

Vector
    The vector to rotate.

Rotation
    Standard rotation value.

Outputs
=======

Vector
    The rotated vector.
